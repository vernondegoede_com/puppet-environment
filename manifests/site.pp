
## site.pp ##

# This file (/etc/puppetlabs/puppet/manifests/site.pp) is the main entry point
# used when an agent connects to a master and asks for an updated configuration.
#
# Global objects like filebuckets and resource defaults should go in this file,
# as should the default node definition. (The default node can be omitted
# if you use the console and don't define any other nodes in site.pp. See
# http://docs.puppetlabs.com/guides/language_guide.html#nodes for more on
# node definitions.)

## Active Configurations ##

# PRIMARY FILEBUCKET
# This configures puppet agent and puppet inspect to back up file contents when
# they run. The Puppet Enterprise console needs this to display file contents
# and differences.

# Define filebucket 'main':
filebucket { 'main':
  server => 'management.vernondegoede.nl',
  path   => false,
}

# Make filebucket 'main' the default backup location for all File resources:
File { backup => 'main' }

###
## Puppet Manifests
###
node default {
  class { '::ntp':
    servers => [ 'time.nist.gov' ],
  }
}


###
## Servers
###
node 'production.vernondegoede.nl', 'development.vernondegoede.nl' {
  class { 'nginx': }
  class { 'yum':
    extrarepo => [ 'epel', 'remi', 'mariadb', 'puppetlabs' ],
  }

  ## MariaDB 
  class { 'mysql::client':
    package_name => 'MariaDB-client',

    require => Class[yum::repo::mariadb]
  }

  class { 'mysql::bindings':
    php_package_name  => 'php-mysqlnd',
  }

  ## PHP 5.4
  class { 'php': 
    service => 'nginx',
    require => Class[yum::repo::remi]
  }

  php::module { "common": }
  php::module { "fpm": }
  php::module { "gd": }
  php::module { "imap": }
  php::module { "intl": }
  php::module { "mcrypt": }
  php::module { "xcache": }
  php::module { "xml": }
  php::module { "xmlrpc": }
}

###
## Management
###
node 'management.vernondegoede.nl' {
  class { 'nginx': }
  class { 'yum':
    extrarepo => [ 'epel', 'remi', 'mariadb', 'puppetlabs' ],
  }

  ## MariaDB 
  class { 'mysql::client':
    package_name => 'MariaDB-client',

    require => Class[yum::repo::mariadb]
  }

  class { 'mysql::bindings':
    php_package_name  => 'php-mysqlnd',
  }

  ## PHP 5.4
  class { 'php': 
    service => 'nginx',
    require => Class[yum::repo::remi]
  }

  php::module { "common": }
  php::module { "fpm": }
  php::module { "gd": }
  php::module { "imap": }
  php::module { "intl": }
  php::module { "mcrypt": }
  php::module { "xcache": }
  php::module { "xml": }
  php::module { "xmlrpc": }
}

###
## Database
###
node 'database.vernondegoede.nl' {
  class { 'yum':
    extrarepo => [ 'epel', 'remi', 'mariadb', 'puppetlabs' ],
  }

  ## MariaDB 
  ## MySQL server
  file { "/var/run/mysqld":
    ensure => "directory",
    owner  => "mysql",
    group  => "mysql",
    mode   => 750,
  }

  class { 'mysql::server':
    package_name       => 'MariaDB-server',
    service_enabled    => true,
    service_manage     => true,
    service_name       => 'mysql',
    manage_config_file => false,
    require            => Class[yum::repo::mariadb]
  }

  class { 'mysql::client':
    package_name => 'MariaDB-client',

    require => Class[yum::repo::mariadb]
  }

  class { 'mysql::bindings':
    php_package_name  => 'php-mysqlnd',
  }
}
